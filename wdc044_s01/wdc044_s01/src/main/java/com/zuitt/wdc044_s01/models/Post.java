package com.zuitt.wdc044_s01.models;

import javax.persistence.*;

//mark this java object as a representation of a database via @Entinty
@Entity
//Designate table name
@Table(name = "posts")
public class Post {


//    properties
    //indicate that this property represents the primary key
    @Id
    //Values for this property will be auto-incremented
    @GeneratedValue
    private Long id; //ito yung primary key

    //class properties that represent table columns in relational database are annotated as @Column
    @Column
    private String title;

    @Column
    private String content;

//    Constructors
    //empty constructor
    public Post () {}

    //parameterized constructor
    public Post(String title, String content){
        this.title = title;
        this.content = content;
    }

//    getters and setters
    public String getTitle(){
        return title;
    }

    public  void setTitle(){
        this.title = title;
    }

    public String getContent(){
        return content;
    }

    public void setContent(){
        this.content = content;
    }
}
