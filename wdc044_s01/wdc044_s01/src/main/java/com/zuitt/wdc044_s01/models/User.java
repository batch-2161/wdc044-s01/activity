package com.zuitt.wdc044_s01.models;


import javax.persistence.*;

@Entity
@Table
public class User {
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String username;

    @Column
    private String password;

//    Constructors
    public User (){}

    public User (String username, String password){
        this.username = username;
        this.password = password;
    }

//    getters and setters
    public String getUsername(){
        return username;
    }

    public void setUsername(){
        this.username=username;
    }

    public String getPassword(){
        return password;
    }

    public void setPassword(){
        this.password = password;
    }

}
