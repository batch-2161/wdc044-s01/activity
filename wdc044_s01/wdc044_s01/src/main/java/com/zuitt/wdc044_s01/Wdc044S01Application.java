package com.zuitt.wdc044_s01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication //Annotations
@RestController //to create end points for our API

public class Wdc044S01Application {

	public static void main(String[] args) {
		SpringApplication.run(Wdc044S01Application.class, args);
	}


	@GetMapping("/hello") //http:/localhost:8080/hello endpoint
	public String hello(@RequestParam(value = "name", defaultValue = "world")String name){
		return String.format("Hello %s!",name); //%s=default value
		//http:/localhost:8080/hello?name=john = Hello john!
	}

	@GetMapping("/hi") //activity
	public String hi(@RequestParam(value = "name", defaultValue = "user")String name){
		return String.format("Hi %s!", name);
	}

	@GetMapping("/nameage") //stretch goal
	public String nameage(@RequestParam(value="name", defaultValue = "Juan") String name, @RequestParam(value="age", defaultValue = "18") String age){
		return String.format("Hi %s!", name) + String.format("Your age is %s", age);
		//String.format("Hello %s! your age is %s", name,age)
		//http:/localhost:8080/nameage?name=john&age=25
	}

}
